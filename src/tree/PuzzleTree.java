package tree;

import java.util.ArrayList;

/**
 *
 * @author Brian Anello
 */
public class PuzzleTree {
    private ArrayList<Integer> start, fin;
    private ArrayList<Node> nodes;
    private Node root;
    private final static int maxH = 10; //This is the variable to change to the depth you want to search.    
    private int currH;
    private int c = 0;
    /**
     * Constructor method for the tree
     * @param initThe initial state
     * @param end  goal state
     */
    PuzzleTree(String init, String end) {
        start = new ArrayList<Integer>();
        fin = new ArrayList<Integer>();
        for(int i = 0; i < 9; ++i) {
            start.add(Character.getNumericValue(init.charAt(i)));
            fin.add(Character.getNumericValue(end.charAt(i)));
        }
        
        root = new Node(start, fin);
        nodes = new ArrayList<Node>();
        currH = 0;
    }
    /**
     * Return the root of the tree
     * @return root
     */
    public Node root() {
        return root;
    }
    /**
     * Return the maximum depth to search
     * @return depth
     */
    public int depth() {
        return maxH;
    }
    /**
     * Find the correct answer if there is one
     * @return Correct answer if there is one
     */
    public String findAns() {
        if(root.findMoves() == true) {
            return root.getMoves();
        } else {
            nodes.add(root);
            return findAnsHelp(nodes);
        }
    }
    /**
     * Helper method for findAns()
     * @param n list of children
     * @return Correct answer if there is one
     */
    public String findAnsHelp(ArrayList<Node> n) {
        ++currH;
        ArrayList<Node> newN = new ArrayList<Node>();
        if(currH <= maxH) {
            for(int i = 0; i < n.size(); ++i) {
                n.get(i).makeBabies();
                for(int j = 0; j < n.get(i).getBabies().size(); ++j) {
                    newN.add(n.get(i).getBabies().get(j));
                }
            }
            for(int i = 0; i < newN.size(); ++i) {                
                if(newN.get(i).findMoves() == true) return newN.get(i).getMoves();
            }
            return findAnsHelp(newN);
        } else {
            return "Search reached limit";
        }
    }
    /**
     * Find the correct answer if there is one in verbose mode
     * @return Correct answer if there is one
     */
    public String findAnsV() {
        if(root.findMoves() == true) {
            System.out.println(root.toString());
            return root.getMoves();
        } else {
            nodes.add(root);
            System.out.println(root.toString());
            return findAnsHelpV(nodes);
        }
    }
    /**
     * Helper method for findAnsV()
     * @param n list of children
     * @return Correct answer if there is one
     */
    public String findAnsHelpV(ArrayList<Node> n) {
        ++currH;
        ArrayList<Node> newN = new ArrayList<Node>();
        if(currH <= maxH) {
            for(int i = 0; i < n.size(); ++i) {
                n.get(i).makeBabies();
                for(int j = 0; j < n.get(i).getBabies().size(); ++j) {
                    newN.add(n.get(i).getBabies().get(j));
                }
            }
            for(int i = 0; i < newN.size(); ++i) {
                System.out.println(newN.get(i).toString());                
                if(newN.get(i).findMoves() == true) return newN.get(i).getMoves();
            }
            return findAnsHelpV(newN);
        } else {
            return "Search reached limit";
        }
    }
    
}
